# Guess the Capital City Game
---
A simple game to test your knowledge of some European capital cities.

###### How it works?
* You are asked to name the capital city of a certain country that the computer chooses and displays in a random order
* If you don't enter an answer, it won't let you continue
* You can use lowercase or uppercase letters
* For every guess, it will display a message that will say whether your answer is correct or wrong
* If you answer correctly, you can continue and a new random country/image is displayed
* If your answer is wrong, you can try again  

## Built With
---

* HTML5
* CSS3
* JavaScript
* [Pixabay](https://pixabay.com) - Photos