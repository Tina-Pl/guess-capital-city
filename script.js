
    // Store coutries + capitals + img
    const countries = [
        {
            country: 'Germany',
            capital: 'Berlin',
            image: 'img/berlin.jpg'
        },
        {
            country: 'Austria',
            capital: 'Vienna',
            image: 'img/vienna.jpg'
        },
        {
            country: 'Italy',
            capital: 'Rome',
            image: 'img/rome.jpg'
        },
        {
            country: 'Slovenia',
            capital: 'Ljubljana',
            image: 'img/ljubljana.jpg'
        },
        {
            country: 'Hungary',
            capital: 'Budapest',
            image: 'img/budapest.jpg'
        },
        {
            country: 'Portugal',
            capital: 'Lisbon',
            image: 'img/lisbon.jpg'
        },
        {
            country: 'France',
            capital: 'Paris',
            image: 'img/paris.jpg'
        },
        {
            country: 'Croatia',
            capital: 'Zagreb',
            image: 'img/zagreb.jpg'
        },
        {
            country: 'United Kingdom',
            capital: 'London',
            image: 'img/london.jpg'
        },
        {
            country: 'Netherlands',
            capital: 'Amsterdam',
            image: 'img/amsterdam.jpg'
        },
        {
            country: 'Poland',
            capital: 'Warsaw',
            image: 'img/warsaw.jpg'
        },
        {
            country: 'Denmark',
            capital: 'Copenhagen',
            image: 'img/copenhagen.jpg'
        },
        {
            country: 'Finland',
            capital: 'Helsinki',
            image: 'img/helsinki.jpg'
        },
        {
            country: 'Sweden',
            capital: 'Stockholm',
            image: 'img/stockholm.jpg'
        },
        {
            country: 'Czechia',
            capital: 'Prague',
            image: 'img/prague.jpg'
        }
    ]

    // Get elements
    const startBtn = document.getElementById('start-btn');
    const guessBtn = document.getElementById('guess-btn');
    const countryName = document.getElementById('country');
    const capitalCityImage = document.getElementById('country-img');
    const input = document.querySelector('.answer');
    const checkAnswer = document.querySelector('.box');
    const answerImg = document.querySelector('.true-false');
    const againBtn = document.getElementById('again-btn');
    const continueBtn = document.getElementById('continue-btn');
    checkAnswer.style.display = "none";

    // Variables
    var randomNum;
    var randomCountry, randomCountryCapital, randomCapitalImage;

    // Start game
    function init() {
        // get random number
        randomNum = Math.floor(Math.random() * countries.length);
        //console.log(randomNum);

        // get randomly chosen country, capital and image
        randomCountry = countries[randomNum].country;
        randomCountryCapital = countries[randomNum].capital;
        randomCapitalImage = countries[randomNum].image;

        // display random country name and image of capital city
        countryName.textContent = randomCountry;
        capitalCityImage.src = randomCapitalImage;
    }

    // After page is loaded execute init function
    document.body.onload = function() { init() };

    // Check if the capital of the randomly chosen country matches the answer of the user
    function checkGuess(){
         guessBtn.addEventListener('click', function() {
            // if input empty
            if(input.value == ""){
                // show alert -> add red border & display 'enter answer' message
                input.classList.add("alert");
                input.placeholder = "Enter answer";

                // on focus remove alert and message
                input.addEventListener('focus', function(){
                    input.classList.remove('alert');
                    input.placeholder = "";
                });

            } else {
                // if input -> change to lowerCase, change the 1st letter to upperCase
                var answerToLower = input.value.toLowerCase();
                var userAnswer = answerToLower.charAt(0).toUpperCase() + answerToLower.substr(1);
                //console.log(userAnswer);

                // check if the answer matches randomly chosen country/capital
                if(userAnswer === randomCountryCapital) {
                    // show check answer box with success message and image
                    checkAnswer.style.display = "block";
                    answerImg.src = "img/right.png";
                    document.querySelector('.message').innerHTML = "Yaaay! Your answer is correct. <br>" + randomCountryCapital + " is indeed the capital of " + randomCountry + ".";

                    // show ContinueBtn and hide Againbtn
                    continueBtn.style.display = "block";
                    againBtn.style.display = "none";

                    // call continueGame function
                    continueGame();

                } else {
                    // show check answer box with fail message and image
                    checkAnswer.style.display = "block";
                    answerImg.src = "img/wrong.png";
                    document.querySelector('.message').innerHTML = "Sorry, but your answer is wrong... <br>  The capital of " + randomCountry + " is not " + userAnswer + ".";

                    // show AgainBtn and hide Continuebtn
                    againBtn.style.display = "block";
                    continueBtn.style.display = "none";

                    // call tryAgain function
                    tryAgain();
                 }
            }
         });
    }
    checkGuess();


    // Try again button
    function tryAgain() {
        againBtn.addEventListener('click', function(){

            // hide check answer box
               checkAnswer.style.display = "none";

            // display the same question and image
               countryName.textContent = randomCountry;
               capitalCityImage.src = randomCapitalImage;

            // clear the input field
               input.value = "";
        });
    }

    // Continue to next question button
    function continueGame() {
        continueBtn.addEventListener('click', function(){

            // hide check answer box
            checkAnswer.style.display = "none";

            // display new country and image -> call init function
            init();

            // clear the input field
            input.value = "";
        });
    }


   












